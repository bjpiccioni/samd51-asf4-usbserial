/*
 * Code generated from Atmel Start.
 *
 * This file will be overwritten when reconfiguring your Atmel Start project.
 * Please copy examples or other code you want to keep to a separate file
 * to avoid losing it when reconfiguring.
 */
#ifndef ATMEL_START_PINS_H_INCLUDED
#define ATMEL_START_PINS_H_INCLUDED

#include <hal_gpio.h>

// SAMD51 has 14 pin functions

#define GPIO_PIN_FUNCTION_A 0
#define GPIO_PIN_FUNCTION_B 1
#define GPIO_PIN_FUNCTION_C 2
#define GPIO_PIN_FUNCTION_D 3
#define GPIO_PIN_FUNCTION_E 4
#define GPIO_PIN_FUNCTION_F 5
#define GPIO_PIN_FUNCTION_G 6
#define GPIO_PIN_FUNCTION_H 7
#define GPIO_PIN_FUNCTION_I 8
#define GPIO_PIN_FUNCTION_J 9
#define GPIO_PIN_FUNCTION_K 10
#define GPIO_PIN_FUNCTION_L 11
#define GPIO_PIN_FUNCTION_M 12
#define GPIO_PIN_FUNCTION_N 13

#define OSC0 GPIO(GPIO_PORTA, 0)
#define OSC1 GPIO(GPIO_PORTA, 1)
#define A0 GPIO(GPIO_PORTA, 2)
#define A3 GPIO(GPIO_PORTA, 4)
#define A1 GPIO(GPIO_PORTA, 5)
#define A2 GPIO(GPIO_PORTA, 6)
#define NC2 GPIO(GPIO_PORTA, 7)
#define FLASH_IO0 GPIO(GPIO_PORTA, 8)
#define FLASH_IO1 GPIO(GPIO_PORTA, 9)
#define FLASH_IO3 GPIO(GPIO_PORTA, 11)
#define PA12 GPIO(GPIO_PORTA, 12)
#define PA13 GPIO(GPIO_PORTA, 13)
#define NC3 GPIO(GPIO_PORTA, 15)
#define D13 GPIO(GPIO_PORTA, 16)
#define D12 GPIO(GPIO_PORTA, 17)
#define D10 GPIO(GPIO_PORTA, 18)
#define D11 GPIO(GPIO_PORTA, 19)
#define D9 GPIO(GPIO_PORTA, 20)
#define D8 GPIO(GPIO_PORTA, 21)
#define D1 GPIO(GPIO_PORTA, 22)
#define D0 GPIO(GPIO_PORTA, 23)
#define Dn GPIO(GPIO_PORTA, 24)
#define Dp GPIO(GPIO_PORTA, 25)
#define TX_LED GPIO(GPIO_PORTA, 27)
#define NC0 GPIO(GPIO_PORTB, 4)
#define NC1 GPIO(GPIO_PORTB, 5)
#define RXLED GPIO(GPIO_PORTB, 6)
#define USB_HOSTEN GPIO(GPIO_PORTB, 7)
#define A4 GPIO(GPIO_PORTB, 8)
#define A5 GPIO(GPIO_PORTB, 9)
#define D7 GPIO(GPIO_PORTB, 12)
#define D4 GPIO(GPIO_PORTB, 13)
#define D5 GPIO(GPIO_PORTB, 14)
#define D6 GPIO(GPIO_PORTB, 15)
#define D3 GPIO(GPIO_PORTB, 16)
#define D2 GPIO(GPIO_PORTB, 17)
#define NEOPIX GPIO(GPIO_PORTB, 22)
#define NC4 GPIO(GPIO_PORTB, 23)

#endif // ATMEL_START_PINS_H_INCLUDED
