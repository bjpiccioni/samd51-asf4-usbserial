#include "sam.h"
#include "board.h"

#include "hal/include/hal_gpio.h"
#include "hal/include/hal_init.h"
#include "hpl/gclk/hpl_gclk_base.h"
#include "hpl_mclk_config.h"

//--------------------------------------------------------------------+
// MACRO TYPEDEF CONSTANT ENUM DECLARATION
//--------------------------------------------------------------------+
#define LED_PIN      16
#define BUTTON_PIN   (14 + 32)  // pin D5

static int  PRESCALER = 1000;
uint32_t    SECONDS = 0;

void board_init(void)
{
  // Clock init ( follow hpl_init.c )
    PRESCALER = 1000;
    SECONDS = 0;
    SysTick_Config(CONF_CPU_FREQUENCY / 1000);
  // Led init
    gpio_set_pin_direction(LED_PIN, GPIO_DIRECTION_OUT);
    gpio_set_pin_level(LED_PIN, 0);

  // Button init
    gpio_set_pin_direction(BUTTON_PIN, GPIO_DIRECTION_IN);
    gpio_set_pin_pull_mode(BUTTON_PIN, GPIO_PULL_UP);
}

//--------------------------------------------------------------------+
// Board porting API
//--------------------------------------------------------------------+

void board_led_write(bool state)
{
  gpio_set_pin_level(LED_PIN, state);
}

uint32_t board_button_read(void)
{
  // button is active low
  return gpio_get_pin_level(BUTTON_PIN) ? 0 : 1;
}

int board_uart_read(uint8_t* buf, int len)
{
  (void) buf; (void) len;
  return 0;
}

int board_uart_write(void const * buf, int len)
{
  (void) buf; (void) len;
  return 0;
}

#if CFG_TUSB_OS  == OPT_OS_NONE
volatile uint32_t system_ticks = 0;


void SysTick_Handler (void)
{
    system_ticks++;
    if( PRESCALER-- == 0 )
    {   
        PRESCALER = 1000; 
        SECONDS++;    
    }    
}

uint32_t board_millis(void)
{
  return system_ticks;
}
#endif
