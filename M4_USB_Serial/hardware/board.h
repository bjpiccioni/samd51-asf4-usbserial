#ifndef _BOARD_H_
#define _BOARD_H_


#include <stdint.h>
#include <stdbool.h>

extern uint32_t    SECONDS;

// Initialize on-board peripherals : led, button, uart and USB
void board_init(void);

// Turn LED on or off
void board_led_write(bool state);

// Get the current state of button
// a '1' means active (pressed), a '0' means inactive.
uint32_t board_button_read(void);

  // Get current milliseconds, must be implemented when no RTOS is used
uint32_t board_millis(void);


static inline void board_led_on(void)
{
  board_led_write(true);
}

static inline void board_led_off(void)
{
  board_led_write(false);
}

// TODO remove
static inline void board_delay(uint32_t ms)
{
  uint32_t start_ms = board_millis();
  while (board_millis() - start_ms < ms){}
}
#endif

